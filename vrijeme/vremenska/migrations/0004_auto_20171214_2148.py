# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-14 21:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vremenska', '0003_auto_20171214_2143'),
    ]

    operations = [
        migrations.AddField(
            model_name='grad',
            name='vrijeme_0_sati_vlaznost',
            field=models.CharField(default=23, max_length=120),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='grad',
            name='vrijeme_12_sati_vlaznost',
            field=models.CharField(default=23, max_length=120),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='grad',
            name='vrijeme_18_sati_vlaznost',
            field=models.CharField(default=23, max_length=120),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='grad',
            name='vrijeme_22_sati_vlaznost',
            field=models.CharField(default=23, max_length=120),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='grad',
            name='vrijeme_6_sati_vlaznost',
            field=models.CharField(default=23, max_length=120),
            preserve_default=False,
        ),
    ]
