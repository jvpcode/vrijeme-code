from django.shortcuts import render,render_to_response
from django.template.context_processors import csrf
from vremenska.models import Grad
def index(request):
    args = {}
    args.update(csrf(request))
    gradovi = Grad.objects.order_by('ime_grada')
    zagreb = Grad.objects.get(ime_grada='Zagreb')
    osijek = Grad.objects.get(ime_grada='Osijek')
    rijeka = Grad.objects.get(ime_grada='Rijeka')
    split = Grad.objects.get(ime_grada='Split')
    dubrovnik = Grad.objects.get(ime_grada='Dubrovnik')
    gradovi2 = [zagreb,osijek,rijeka,split,dubrovnik]
    args['gradovi'] = gradovi
    args['gradovi2'] = gradovi2
    return render_to_response('vremenska/index.html',args)
def trazi_gradove(request):
    if request.method == 'POST':
        search_text = request.POST['search_text']
    else:
        search_text = ''
    d_gradovi = Grad.objects.filter(ime_grada__contains=search_text)
    return render_to_response('vremenska/ajax_search.html',{'d_gradovi':d_gradovi})
def sitemap(request):
    return render(request,'vremenska/sitemap.xml')
def grad(request,grad_ime):
    args = {}
    args.update(csrf(request))
    gradovi = Grad.objects.order_by('ime_grada')
    grad = Grad.objects.get(ime_grada=grad_ime)
    lista = ['1:2','2:3','3:4','4:5','5:6','6:7','7:8','8:9','9:10']
    args['lista'] = lista
    args['grad'] = grad
    args['gradovi'] = gradovi
    return render_to_response('vremenska/grad.html',args)
def sutra(request):
    args = {}
    args.update(csrf(request))
    gradovi = Grad.objects.order_by('ime_grada')
    zagreb = Grad.objects.get(ime_grada='Zagreb')
    osijek = Grad.objects.get(ime_grada='Osijek')
    rijeka = Grad.objects.get(ime_grada='Rijeka')
    split = Grad.objects.get(ime_grada='Split')
    dubrovnik = Grad.objects.get(ime_grada='Dubrovnik')
    gradovi2 = [zagreb,osijek,rijeka,split,dubrovnik]
    args['gradovi'] = gradovi
    args['gradovi2'] = gradovi2
    return render_to_response('vremenska/sutra.html',args)

def google(request):
    return render(request,'vremenska/google73cdd85a4713cc07.html')
