# -*- coding: utf-8 -*-

from django.db import models
import datetime


class Grad(models.Model):
    ime_grada=models.CharField(max_length=320)
    id_grada=models.CharField(max_length=320)
    zupanija=models.CharField(max_length=320)
    drzava=models.CharField(max_length=320)
    datum=models.CharField(max_length=320)
    izlazak=models.CharField(max_length=320)
    zalazak=models.CharField(max_length=320)
    trenutno_vrijeme=models.CharField(max_length=320)
    trenutno_vrijeme_ikona=models.CharField(max_length=320)
    trenutna_temperatura=models.CharField(max_length=320)
    trenutna_vlaznost=models.CharField(max_length=320)
    trenutna_kisa=models.CharField(max_length=320)
    danas_maksimalna=models.CharField(max_length=320)
    trenutna_snijeg=models.CharField(max_length=320)
    danas_minimalna=models.CharField(max_length=320)
    maximalna_temperatura=models.CharField(max_length=320)
    minimalna_temperatura=models.CharField(max_length=320)
    max_vjetar=models.CharField(max_length=320)
    padaline=models.CharField(max_length=320)
    vlaga=models.CharField(max_length=320)
    vrijeme_dan=models.CharField(max_length=320)
    vrijeme_dan_ikona=models.CharField(max_length=320)
    vlaga=models.CharField(max_length=320)
    vrijeme_0_sati_temp=models.CharField(max_length=320)
    vrijeme_0_sati_vrijeme=models.CharField(max_length=320)
    vrijeme_0_sati_vrijeme_ikona=models.CharField(max_length=320)
    vrijeme_0_sati_vjetar_brzina=models.CharField(max_length=320)
    vrijeme_0_sati_vjetar_smjer=models.CharField(max_length=320)
    vrijeme_0_sati_padaline=models.CharField(max_length=320)
    vrijeme_0_sati_sansa_za_kisu=models.CharField(max_length=320)
    vrijeme_0_sati_sansa_za_snijeg=models.CharField(max_length=320)
    vrijeme_0_sati_vlaznost=models.CharField(max_length=320)
    vrijeme_6_sati_temp=models.CharField(max_length=320)
    vrijeme_6_sati_vrijeme=models.CharField(max_length=320)
    vrijeme_6_sati_vrijeme_ikona=models.CharField(max_length=320)
    vrijeme_6_sati_vjetar_brzina=models.CharField(max_length=320)
    vrijeme_6_sati_vjetar_smjer=models.CharField(max_length=320)
    vrijeme_6_sati_padaline=models.CharField(max_length=320)
    vrijeme_6_sati_sansa_za_kisu=models.CharField(max_length=320)
    vrijeme_6_sati_sansa_za_snijeg=models.CharField(max_length=320)
    vrijeme_6_sati_vlaznost=models.CharField(max_length=320)
    vrijeme_12_sati_temp=models.CharField(max_length=320)
    vrijeme_12_sati_vrijeme=models.CharField(max_length=320)
    vrijeme_12_sati_vrijeme_ikona=models.CharField(max_length=320)
    vrijeme_12_sati_vjetar_brzina=models.CharField(max_length=320)
    vrijeme_12_sati_vjetar_smjer=models.CharField(max_length=320)
    vrijeme_12_sati_padaline=models.CharField(max_length=320)
    vrijeme_12_sati_sansa_za_kisu=models.CharField(max_length=320)
    vrijeme_12_sati_sansa_za_snijeg=models.CharField(max_length=320)
    vrijeme_12_sati_vlaznost=models.CharField(max_length=320)
    vrijeme_18_sati_temp=models.CharField(max_length=320)
    vrijeme_18_sati_vrijeme=models.CharField(max_length=320)
    vrijeme_18_sati_vrijeme_ikona=models.CharField(max_length=320)
    vrijeme_18_sati_vjetar_brzina=models.CharField(max_length=320)
    vrijeme_18_sati_vjetar_smjer=models.CharField(max_length=320)
    vrijeme_18_sati_padaline=models.CharField(max_length=320)
    vrijeme_18_sati_sansa_za_kisu=models.CharField(max_length=320)
    vrijeme_18_sati_sansa_za_snijeg=models.CharField(max_length=320)
    vrijeme_18_sati_vlaznost=models.CharField(max_length=320)
    vrijeme_22_sati_temp=models.CharField(max_length=320)
    vrijeme_22_sati_vrijeme=models.CharField(max_length=320)
    vrijeme_22_sati_vrijeme_ikona=models.CharField(max_length=320)
    vrijeme_22_sati_vjetar_brzina=models.CharField(max_length=320)
    vrijeme_22_sati_vjetar_smjer=models.CharField(max_length=320)
    vrijeme_22_sati_padaline=models.CharField(max_length=320)
    vrijeme_22_sati_sansa_za_kisu=models.CharField(max_length=320)
    vrijeme_22_sati_sansa_za_snijeg=models.CharField(max_length=320)
    vrijeme_22_sati_vlaznost=models.CharField(max_length=320)

    def __str__(self):
        return self.ime_grada
    def maximalna_temperatura_as_list(self):
        return self.maximalna_temperatura.strip('[').strip(']').split(',')
    def vrijeme_dan_ikona_as_list(self):
        return self.vrijeme_dan_ikona.split(',')
    def datum_as_list(self):
        return self.datum.split(',') 
    def izlazak_as_list(self):
        return self.izlazak.split(',')
    def zalazak_as_list(self):
        return self.zalazak.split(',')
    def minimalna_temperatura_as_list(self):
        return self.minimalna_temperatura.strip('[').strip(']').split(',')
    def max_vjetar_as_list(self):
        return self.max_vjetar.strip('[').strip(']').split(',')
    def vrijeme_dan_as_list(self):
        return self.vrijeme_dan.strip('[').strip(']').split(',')
    def padaline_as_list(self):
        return self.padaline.strip('[').strip(']').split(',')
    def vrijeme_22_sati_temp_as_list(self):
        return self.vrijeme_22_sati_temp.strip('[').strip(']').split(',')
    def vrijeme_22_sati_vrijeme_as_list(self):
        return self.vrijeme_22_sati_vrijeme.strip('[').strip(']').split(',')
    def vrijeme_22_sati_vrijeme_ikona_as_list(self):
        return self.vrijeme_22_sati_vrijeme_ikona.strip('[').strip(']').split(',')
    def vrijeme_22_sati_vjetar_brzina_as_list(self):
        return self.vrijeme_22_sati_vjetar_brzina.strip('[').strip(']').split(',')
    def vrijeme_22_sati_vjetar_smjer_as_list(self):
        return self.vrijeme_22_sati_vjetar_smjer.strip('[').strip(']').split(',')
    def vrijeme_22_sati_padaline_as_list(self):
        return self.vrijeme_22_sati_padaline.strip('[').strip(']').split(',')
    def vrijeme_22_sati_sansa_za_kisu_as_list(self):
        return self.vrijeme_22_sati_sansa_za_kisu.strip('[').strip(']').split(',')
    def vrijeme_22_sati_sansa_za_snijeg_as_list(self):
        return self.vrijeme_22_sati_sansa_za_snijeg.strip('[').strip(']').split(',')
    def vrijeme_22_sati_vlaznost_as_list(self):
        return self.vrijeme_22_sati_vlaznost.strip('[').strip(']').split(',')
    def vrijeme_0_sati_temp_as_list(self):
        return self.vrijeme_0_sati_temp.strip('[').strip(']').split(',')
    def vrijeme_0_sati_vrijeme_as_list(self):
        return self.vrijeme_0_sati_vrijeme.strip('[').strip(']').split(',')
    def vrijeme_0_sati_vrijeme_ikona_as_list(self):
        return self.vrijeme_0_sati_vrijeme_ikona.strip('[').strip(']').split(',')
    def vrijeme_0_sati_vjetar_brzina_as_list(self):
        return self.vrijeme_0_sati_vjetar_brzina.strip('[').strip(']').split(',')
    def vrijeme_0_sati_vjetar_smjer_as_list(self):
        return self.vrijeme_0_sati_vjetar_smjer.strip('[').strip(']').split(',')
    def vrijeme_0_sati_padaline_as_list(self):
        return self.vrijeme_0_sati_padaline.strip('[').strip(']').split(',')
    def vrijeme_0_sati_sansa_za_kisu_as_list(self):
        return self.vrijeme_0_sati_sansa_za_kisu.strip('[').strip(']').split(',')
    def vrijeme_0_sati_sansa_za_snijeg_as_list(self):
        return self.vrijeme_0_sati_sansa_za_snijeg.strip('[').strip(']').split(',')
    def vrijeme_0_sati_vlaznost_as_list(self):
        return self.vrijeme_0_sati_vlaznost.strip('[').strip(']').split(',')
    def vrijeme_6_sati_temp_as_list(self):
        return self.vrijeme_0_sati_temp.strip('[').strip(']').split(',')
    def vrijeme_6_sati_vrijeme_as_list(self):
        return self.vrijeme_6_sati_vrijeme.strip('[').strip(']').split(',')
    def vrijeme_6_sati_vrijeme_ikona_as_list(self):
        return self.vrijeme_6_sati_vrijeme_ikona.strip('[').strip(']').split(',')
    def vrijeme_6_sati_vjetar_brzina_as_list(self):
        return self.vrijeme_6_sati_vjetar_brzina.strip('[').strip(']').split(',')
    def vrijeme_6_sati_vjetar_smjer_as_list(self):
        return self.vrijeme_6_sati_vjetar_smjer.strip('[').strip(']').split(',')
    def vrijeme_6_sati_padaline_as_list(self):
        return self.vrijeme_6_sati_padaline.strip('[').strip(']').split(',')
    def vrijeme_6_sati_sansa_za_kisu_as_list(self):
        return self.vrijeme_6_sati_sansa_za_kisu.strip('[').strip(']').split(',')
    def vrijeme_6_sati_sansa_za_snijeg_as_list(self):
        return self.vrijeme_6_sati_sansa_za_snijeg.strip('[').strip(']').split(',')
    def vrijeme_6_sati_vlaznost_as_list(self):
        return self.vrijeme_6_sati_vlaznost.strip('[').strip(']').split(',')
    def vrijeme_12_sati_temp_as_list(self):
        return self.vrijeme_12_sati_temp.strip('[').strip(']').split(',')
    def vrijeme_12_sati_vrijeme_as_list(self):
        return self.vrijeme_12_sati_vrijeme.strip('[').strip(']').split(',')
    def vrijeme_12_sati_vrijeme_ikona_as_list(self):
        return self.vrijeme_12_sati_vrijeme_ikona.strip('[').strip(']').split(',')
    def vrijeme_12_sati_vjetar_brzina_as_list(self):
        return self.vrijeme_12_sati_vjetar_brzina.strip('[').strip(']').split(',')
    def vrijeme_12_sati_vjetar_smjer_as_list(self):
        return self.vrijeme_12_sati_vjetar_smjer.strip('[').strip(']').split(',')
    def vrijeme_12_sati_padaline_as_list(self):
        return self.vrijeme_12_sati_padaline.strip('[').strip(']').split(',')
    def vrijeme_12_sati_sansa_za_kisu_as_list(self):
        return self.vrijeme_12_sati_sansa_za_kisu.strip('[').strip(']').split(',')
    def vrijeme_12_sati_sansa_za_snijeg_as_list(self):
        return self.vrijeme_12_sati_sansa_za_snijeg.strip('[').strip(']').split(',')
    def vrijeme_12_sati_vlaznost_as_list(self):
        return self.vrijeme_12_sati_vlaznost.strip('[').strip(']').split(',')
    def vrijeme_18_sati_temp_as_list(self):
        return self.vrijeme_18_sati_temp.strip('[').strip(']').split(',')
    def vrijeme_18_sati_vrijeme_as_list(self):
        return self.vrijeme_18_sati_vrijeme.strip('[').strip(']').split(',')
    def vrijeme_18_sati_vrijeme_ikona_as_list(self):
        return self.vrijeme_18_sati_vrijeme_ikona.strip('[').strip(']').split(',')
    def vrijeme_18_sati_vjetar_brzina_as_list(self):
        return self.vrijeme_18_sati_vjetar_brzina.strip('[').strip(']').split(',')
    def vrijeme_18_sati_vjetar_smjer_as_list(self):
        return self.vrijeme_18_sati_vjetar_smjer.strip('[').strip(']').split(',')
    def vrijeme_18_sati_padaline_as_list(self):
        return self.vrijeme_18_sati_padaline.strip('[').strip(']').split(',')
    def vrijeme_18_sati_sansa_za_kisu_as_list(self):
        return self.vrijeme_18_sati_sansa_za_kisu.strip('[').strip(']').split(',')
    def vrijeme_18_sati_sansa_za_snijeg_as_list(self):
        return self.vrijeme_18_sati_sansa_za_snijeg.strip('[').strip(']').split(',')
    def vrijeme_18_sati_vlaznost_as_list(self):
        return self.vrijeme_18_sati_vlaznost.strip('[').strip(']').split(',')
      
      
      
      
   
 	
# Create your models here.