import pyowm
 # -*- coding: utf-8 -*- 
import pickle
from vremenska.models import Grad
import time
"""
Script for updating OWM data
"""
opis_vremena={'Sunny':'Sunčano','Clear':'Vedro','Partly cloudy':'Djelomična Naoblaka','Cloudy':'Oblačno','Overcast':'Naoblaka','Mist':'Magla','Patchy rain possible':'Mjestimična kiša','Patchy snow possible':'Mjestimičan snijeg','Patchy sleet possible':'Mjestimični kiša i snijeg','Patchy freezing drizzle possible':'Mjestimice ledena kiša','Thundery outbreaks possible':'Grmljavina','Blowing snow':'Nanosi snijega','Blizzard':'Snježno nevrijeme','Fog':'Magla',
'Freezing fog':'Magla','Patchy light drizzle':'Slaba kiša moguća','Light drizzle':'Slaba kiša','Freezing drizzle':'Kiša koja se smrzava na tlu','Heavy freezing drizzle':'Kiša koja se smrzava na tlu','Patchy light rain':'Mjestimična lagana kiša','Light rain':'Lagana kiša','Moderate rain at times':'Povremena umjerena kiša','Moderate rain':'Umjerena kiša',
'Heavy rain at times':'Povremeni jaki pljuskovi','Heavy rain':'Jaki Pljuskovi','Light freezing rain':'Lagana ledena kiša','Moderate or heavy freezing rain':'Umjerena do jaka kiša sa smrzavanjem na tlu','Light sleet':'Lagana kiša','Moderate or heavy sleet':'Umjerena ledena kiša','Patchy light snow':'Mjestimičan lagan snijeg',
'Light snow':'Lagan snijeg','Patchy moderate snow':'Mjestimičan umjeren snijeg','Moderate snow':'Umjeren snijeg','Patchy heavy snow':'Mjestimice jak snijeg','Heavy snow':'Jak snijeg','Ice pellets':'Led','Light rain shower':'Lagani pljuskovi','Moderate or heavy rain shower':'Umjereni pljuskovi',
'Torrential rain shower':'Pljuskovi','Light sleet showers':'Lagani ledeni pljuskovi','Moderate or heavy sleet showers':'Umjereni ledeni pljuskovi' ,'Light snow showers':'Lagani snježni pljuskovi','Moderate or heavy snow showers':'Umjereni snježni pljuskovi',
'Light showers of ice pellets':'Lagani ledeni pljuskovi','Moderate or heavy showers of ice pellets':'Umjereni ledeni pljuskovi','Patchy light rain with thunder':'Mjestimična kiša s grmljavinom','Moderate or heavy rain with thunder':'Kiša s grmljavinom','Patchy light snow with thunder':'Mjestimični snijeg i grmljavina','Moderate or heavy snow with thunder':'Umjeren ili jak snijeg s grmljavinom'}

opis_vremena_owm={'clear sky':'Clear','few clouds':'Partly cloudy','scattered clouds':'Partly cloudy','broken clouds':'Partly cloudy','shower rain':'Light rain shower','rain':'Moderate or heavy rain shower','thunderstorm':'Thundery outbreaks possible','snow':'Moderate snow','mist':'Mist','thunderstorm with light rain':'Patchy light rain with thunder','thunderstorm with rain':'Moderate or heavy rain with thunder'
,'thunderstorm with heavy rain':'Moderate or heavy rain with thunder','light thunderstorm':'Thundery outbreaks possible','thunderstorm':'Thundery outbreaks possible','heavy thunderstorm':'Thundery outbreaks possible','ragged thunderstorm':'Thundery outbreaks possible','thunderstorm with light drizzle':'Moderate or heavy rain with thunder','thunderstorm with drizzle':'Moderate or heavy rain with thunder','thunderstorm with heavy drizzle':'Moderate or heavy rain with thunder'
,'light intensity drizzle':'Light drizzle','drizzle':'Light drizzle','heavy intensity drizzle':'Freezing drizzle','light intensity drizzle rain':'Freezing drizzle','drizzle rain':'Light drizzle','heavy intensity drizzle rain':'Light drizzle','shower rain and drizzle':'Light drizzle','heavy shower rain and drizzle':'Moderate or heavy rain shower' ,'shower drizzle':'Moderate or heavy rain shower','light rain ':'Light rain','moderate rain':'Moderate rain'
,'heavy intensity rain':'Heavy rain','very heavy rain':'Heavy rain','extreme rain':'Heavy rain','freezing rain':'Moderate or heavy freezing rain','light intensity shower rain':'Moderate or heavy rain shower','shower rain':'Moderate or heavy rain shower','heavy intensity shower rain':'Moderate or heavy rain shower','ragged shower rain':'Moderate or heavy rain shower','light snow':'Light snow','snow':'Moderate snow','heavy snow':'Heavy snow'
,'sleet':'Moderate or heavy sleet','shower sleet':'Moderate or heavy sleet showers'
,'light rain and snow':'Moderate or heavy freezing rain','rain and snow':'Moderate or heavy freezing rain','light shower snow':'Light snow showers','shower snow':'Moderate or heavy snow showers','heavy shower snow':'Moderate or heavy snow showers','mist':'Mist','haze':'Cloudy','fog':'Fog','clear sky':'Clear','few clouds':'Partly cloudy','overcast clouds':'Cloudy','tropical storm':'Moderate or heavy rain with thunder'
,'hurricane':'Moderate or heavy rain with thunder','cold':'Cloudy','hot':'Sunny','windy':'Cloudy','hail':'Ice pellets','calm':'Partly cloudy','light breeze':'Partly cloudy','gentle breeze':'Partly cloudy','moderate breeze':'Cloudy','fresh breeze':'Cloudy','strong breeze':'Cloudy'}

brojevi_ikona={'Sunny': '113', 'Clear': '113', 'Partly cloudy': '116', 'Cloudy': '119', 'Overcast': '122', 'Mist': '143', 'Patchy rain possible': '176', 'Patchy snow possible': '179', 'Patchy sleet possible': '182', 'Patchy freezing drizzle possible': '185', 'Thundery outbreaks possible': '200', 'Blowing snow': '227', 'Blizzard': '230', 'Fog': '248', 'Freezing fog': '260', 'Patchy light drizzle': '263', 'Light drizzle': '266', 'Freezing drizzle': '281', 'Heavy freezing drizzle': '284', 'Patchy light rain': '293', 'Light rain': '296', 'Moderate rain at times': '299', 'Moderate rain': '302', 'Heavy rain at times': '305', 'Heavy rain': '308', 'Light freezing rain': '311', 'Moderate or heavy freezing rain': '314', 'Light sleet': '317', 'Moderate or heavy sleet': '320', 'Patchy light snow': '323', 'Light snow': '326', 'Patchy moderate snow': '329', 'Moderate snow': '332', 'Patchy heavy snow': '335', 'Heavy snow': '338', 'Ice pellets': '350', 'Light rain shower': '353', 'Moderate or heavy rain shower': '356', 'Torrential rain shower': '359', 'Light sleet showers': '362', 'Moderate or heavy sleet showers': '365', 'Light snow showers': '368', 'Moderate or heavy snow showers': '371', 'Light showers of ice pellets': '374', 'Moderate or heavy showers of ice pellets': '377', 'Patchy light rain with thunder': '386', 'Moderate or heavy rain with thunder': '389', 'Patchy light snow with thunder': '392', 'Moderate or heavy snow with thunder': '395'}

owm = pyowm.OWM('9ead5ef2#######aa31e')
lista_gradova=pickle.load(open('lista_hrvatskih_gradova.p','rb'))
for grad in lista_gradova:
    id_grada=int(grad.split()[0])
    ime_grada=grad.split()[1]
    print (ime_grada)
    observation = owm.weather_at_id(id_grada)
    w = observation.get_weather()
    trenutno_vrijeme=w.get_detailed_status()
    trenutno_vrijeme_ap=opis_vremena_owm[trenutno_vrijeme]
    trenutno_vrijeme=opis_vremena[trenutno_vrijeme_ap]
    broj_ikone=brojevi_ikona[trenutno_vrijeme_ap]+'.png'
    trenutna_temperatura=w.get_temperature(unit='celsius')['temp']
    trenutna_maximalna=w.get_temperature(unit='celsius')['temp_max']
    trenutna_minimalna=w.get_temperature(unit='celsius')['temp_min']
    ikona=w.get_weather_icon_name()
    if 'd' in ikona:
        status='/day/'
    else:
        status='/night/'
    trenutno_vrijeme_ikona=status+broj_ikone
#s=('/night/119.png')
    trenutna_vlaznost=w.get_humidity()
    if w.get_rain():
        trenutna_kisa=w.get_rain()
    else:
        trenutna_kisa=0
    if w.get_snow():
        trenutna_snijeg=w.get_snow()
    else:
        trenutna_snijeg=0

    grad_za_unos=Grad.objects.get(id_grada=id_grada)
    grad_za_unos.trenutno_vrijeme=trenutno_vrijeme
    grad_za_unos.trenutno_vrijeme_ikona=trenutno_vrijeme_ikona
    grad_za_unos.trenutna_temperatura=trenutna_temperatura
    grad_za_unos.trenutna_vlaznost=trenutna_vlaznost
    grad_za_unos.trenutna_kisa=trenutna_kisa
    grad_za_unos.trenutna_vlaznost=trenutna_vlaznost
    grad_za_unos.trenutna_temperatura=trenutna_temperatura
    grad_za_unos.trenutna_snijeg=trenutna_snijeg
    grad_za_unos.danas_maksimalna=trenutna_maximalna
    grad_za_unos.danas_minimalna=trenutna_minimalna
    grad_za_unos.save()
    time.sleep(1)